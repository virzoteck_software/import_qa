# -*- coding: utf-8 -*-
{
    'name': 'Imprort survey,question,answer by excelsheet',
    'category': 'Marketing',
    'website': '',
    'summary': '',
    'author':'virzoteck software and solutions(virzoteck@gmail.com)',
    'description': """
    Imprort survey,question,answer by excelsheet in odoo with less efforts and minimal data entry
    """,
    'depends': [
       'survey'
    ],
    'data': [
        'views/import_question_tool.xml'
        
    ],
    'demo': [],
        "price":18,
    "currency": "EUR",
        'images': ['static/description/background.png',],
    'installable': True,
    'application': True,
}
