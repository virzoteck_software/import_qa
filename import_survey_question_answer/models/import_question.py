# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import xlrd
import itertools
import base64
from odoo.exceptions import UserError, ValidationError


class ImportQuestionTool(models.TransientModel):
    _name = 'import.question.tool'
    
    def _default_subtitle(self):
        subtitle="""<div>
                <p class="terms">File Should contain</p>
                <ul><li>Unique Question Id</li>
                <li>Value of level should be in a1,a2,b1,b2,c1</li>
                <li>Value of language should be similar to ISO code  field of odoo's Language Model,example en,it,fr..</li>
                <li>Value of Type of answer should be from simple_choice and multipe_choice</li>
                <li>Value of Question Type from vocabulary,grammar,comprehension</li>
                <li>There Should be Sheet2 with Question Id and Answer</li><ul/>
                </div>"""
        return subtitle
    
    def _default_retrive_sample_file(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        file_link=base_url+'/import_survey_question_answer/static/description/sample.xls'
        return file_link
    
    xls_file = fields.Binary(string='XLS File',required=True)
    subtitle = fields.Html(string='Note',default=_default_subtitle,readonly=True)
    sample_file_link=fields.Char("Download sample File",default=_default_retrive_sample_file,readonly=True)
    survey_page = fields.Many2one('survey.page','Survey Page')

    

    @api.multi
    def import_questions(self):
        survey_question_obj=self.env['survey.question']
        language_obj=self.env['res.lang']
        survey_label_obj=self.env['survey.label']
        
        file=base64.b64decode(self.xls_file)

        wb = xlrd.open_workbook(file_contents=file)
        
        try:
            #import Sheet 1
            sheet = wb.sheet_by_index(0)
            row_count=0
            for row in list(map(sheet.row, range(sheet.nrows))):
                row_count+=1
                if row_count==1:
                    continue    
                language_id=language_obj.search([('iso_code','=',row[2].value.lower())])
                if not len(language_id):
                    language_id=language_obj.search([('iso_code','=','en')])

                unique_question_id=survey_question_obj.search([('question_id','=',row[0].value)])
                if len(unique_question_id):
                    raise UserError(_("%s question id is not unique,question id in first sheet should be unique.") % row[0].value)

                try:    
                    survey_question_obj.create({'question_id':row[0].value,'question':row[1].value,'lang_id':language_id.id,'level':row[3].value.lower(),'question_type':row[4].value.lower(),'type':row[5].value.lower(),'page_id':self.survey_page.id})
                except Exception as e:
                    raise UserError(_("%s") % str(e))
                
            #import sheet 2   
            sheet = wb.sheet_by_index(1)
            row_count=0
            for row in list(map(sheet.row, range(sheet.nrows))):
                row_count+=1
                if row_count==1:
                    continue    
                question_id=survey_question_obj.search([('question_id','=',row[0].value)])
                if  len(question_id):
                    try:
                        survey_label_obj.create({'question_id':question_id.id,'value':row[1].value})
                    except Exception as e:
                        raise UserError(_("%s") % str(e))  
                    
        except Exception as e:
            raise UserError(_("%s") % str(e))    
        return True

class SurveyQuestion(models.Model):
    _inherit = 'survey.question'

    question_id=fields.Char(string="Question Id")